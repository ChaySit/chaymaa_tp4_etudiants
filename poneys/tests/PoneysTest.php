<?php
use PHPUnit\Framework\TestCase;

require_once 'src/Poneys.php';

class PoneysTest extends TestCase
{
    public function test_removePoneyFromField()
    {
        // Setup
        $Poneys = new Poneys();

        // Action
        $Poneys->removePoneyFromField(3);

        // Assert
        $this->assertEquals(5, $Poneys->getCount());
    }
}
?>
